#include "../include/deserializer.h"

#define Pi 180
#define Pi_2 (Pi / 2)
uint32_t rotate_calculator(int64_t radian) {
    if (radian < 0) {
        radian = radian + 2 * Pi;
    }

    uint32_t count_of_rotate = radian / Pi_2;
    return count_of_rotate;
}

struct image rotate(const struct image* img) {
    uint64_t width = (*img).width;
    uint64_t height = (*img).height;

    struct image rotated_img;
    struct pixel* rotated_data = malloc(3 * height * width);
    image_init(&rotated_img, height, width, rotated_data);

    for (uint64_t i = 1; i <= height; i++) {
        for (uint64_t j = 1; j <= width; j++) {
            uint64_t pixel_address = width * (i - 1) + (j - 1);
            uint64_t rotated_pixel_address = height * width - (j * height - i) - 1;

            rotated_img.data[rotated_pixel_address] = (*img).data[pixel_address];
        }
    }
    free(img->data);
    return rotated_img;
}

struct image rotator(struct image* source, int64_t radian) {
    uint32_t several_times = rotate_calculator(radian);
    struct image rotated = *source;
    for (uint32_t t = 0; t < several_times; t++) {
        rotated = rotate(&rotated);
    }
    return rotated;
}
