#include "../include/deserializer.h"
#include "../include/rotator.h"
#include <stdio.h>
#include <stdlib.h>

uint8_t validate_radian(int64_t radian) {
    if ((-270 <= radian) && (radian <= 270) && (radian % 90 == 0)) {
        return 1;
    }
    else {
        return 0;
    }
}


int main(int argc, char** argv ) {
    char* end_of_str;
    if (argc != 4) { return 1;}

    int64_t radian = strtol(argv[3], &end_of_str, 10);
    uint8_t radian_is_valid = validate_radian(radian);
    if (radian_is_valid == 0) {
        fprintf(stderr, "Неверный угол, завершаю программу!");
        return -1;
    }

    //printf("%" PRIu64 "\n", radian);

    FILE* ptr_for_read;
    FILE* ptr_for_write;

    ptr_for_read = fopen(argv[1], "rb");
    ptr_for_write = fopen(argv[2], "wb");

    struct image input_image;
    struct image output_image;

    from_bmp(ptr_for_read, &input_image);
    //printf("%" PRIu8 "\n", input_image.data[0].green);

    output_image = rotator(&input_image, radian);

    //printf("%" PRIu8 "\n", output_image.data[0].green);

    enum write_status prog_state = to_bmp(ptr_for_write, &output_image);
    if (prog_state == WRITE_ERROR) {
        fprintf(stderr, "Ошибка записи в файл, завершаю программу.");
        return -1;
    }
    //printf("%" PRIu8 "\n", output_image.data[0].green);

    //printf("%" PRIu64 "\n", input_image.width);
    //printf("%" PRIu64 "\n", input_image.height);
    free(output_image.data);

    return 0;
}
