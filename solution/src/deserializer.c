#include "../include/bmp_header.h"
#include "../include/bmp_params_calculator.h"
#include "../include/deserializer.h"
#include <stdio.h>
#include <stdlib.h>
#define BMP_TYPE_SIGN 0x4d42
#define BMP_TYPE_OFFSET_OF_DOTS 54
#define BMP_TYPE_BYTE_SIZE_OF_HEADERS 40
#define COLOR_COUNT 3
#define BIT_IN_BYTE 8

static void zeroficate_data(uint8_t *zeroficator, uint32_t i_size) {
    for (uint64_t i = 0; i < i_size; i++) {
        *(zeroficator + i) = 0;
    }
}

void image_init(struct image* img, uint64_t width, uint64_t height, struct pixel* data) {
    (*img).width = width;
    (*img).height = height;
    (*img).data = data;
}

void image_save(struct image* image, uint32_t w, uint32_t h, uint32_t p, uint8_t *reserved_data) {
    for (uint64_t r = 0; r < h; r++) {
        for (uint64_t c = 0; c < w; c++) {
            uint32_t current_address = r * w + c;
            uint32_t entity_address = current_address_from_coordinates(w, p, r, c);

            struct pixel new_pixel = {0};

            uint32_t blue_pixel_address = entity_address;
            uint32_t green_pixel_address = entity_address + 1;
            uint32_t red_pixel_address = entity_address + 2;

            new_pixel.blue = reserved_data[blue_pixel_address];
            new_pixel.green = reserved_data[green_pixel_address];
            new_pixel.red = reserved_data[red_pixel_address];
            (*image).data[current_address] = new_pixel;
        }
    }
    free(reserved_data);
}

static void image_load(struct image const* image, uint32_t w, uint32_t h, uint32_t p, uint8_t *reserved_data) {
    for (uint64_t r = 0; r < h; r++) {
        for (uint64_t c = 0; c < w; c++) {
            uint32_t current_address = current_address_from_coordinates(w, p, r, c);
            //printf("%" PRIu32 "\n", current_address);
            uint32_t entity_address = r * w + c;
            uint32_t red_pixel_address = current_address + 2;
            uint32_t green_pixel_address = current_address + 1;
            uint32_t blue_pixel_address = current_address;

            uint8_t red_pixel = (*image).data[entity_address].red;
            uint8_t green_pixel = (*image).data[entity_address].green;
            uint8_t blue_pixel = (*image).data[entity_address].blue;

            //printf("%" PRIu8 ":" "%" PRIu8 " ", reserved_data[red_pixel_address], red_pixel);
            //printf("%" PRIu8 ":" "%" PRIu8 " ", reserved_data[green_pixel_address], green_pixel);
            //printf("%" PRIu8 ":" "%" PRIu8 " ", reserved_data[blue_pixel_address],  blue_pixel);

            reserved_data[red_pixel_address] = red_pixel;
            reserved_data[green_pixel_address] = green_pixel;
            reserved_data[blue_pixel_address] = blue_pixel;

        }
    }
}

struct bmp_header header_create(struct image const *image) {
    uint32_t bmp_content_size = calculate_image_size((*image).width, (*image).height);
    struct bmp_header header = {
            .bfType = BMP_TYPE_SIGN,
            .bfileSize = BMP_TYPE_OFFSET_OF_DOTS + bmp_content_size,
            .bfReserved = 0,
            .bOffBits = BMP_TYPE_OFFSET_OF_DOTS,
            .biSize = BMP_TYPE_BYTE_SIZE_OF_HEADERS,
            .biWidth = (*image).width,
            .biHeight = (*image).height,
            .biPlanes = 1,
            .biBitCount = COLOR_COUNT * BIT_IN_BYTE,
            .biCompression = 0,
            .biSizeImage = bmp_content_size,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
    return header;
}

enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header headers = {0};
    fseek(in, 0, 0);
    fread(&headers, sizeof(struct bmp_header), 1, in);

    struct pixel* data = malloc(3 * headers.biWidth * headers.biHeight);
    uint8_t* reserved_data = malloc(headers.biSizeImage);
    fseek(in, (long) headers.bOffBits, SEEK_SET);
    fread(reserved_data, headers.biSizeImage, 1, in);

    uint32_t padding = calculate_bytes_padding(headers.biWidth);

    image_init(img, headers.biWidth, headers.biHeight, data);
    image_save(img, (*img).width, (*img).height, padding, reserved_data);

    return 0;
}

enum write_status to_bmp(FILE* out, struct image* img) {
    uint32_t padding = calculate_bytes_padding((*img).width);

    struct bmp_header header = header_create(img);

    if (!fwrite(&header, sizeof(struct bmp_header), 1, out)) {
        return WRITE_ERROR;
    }

    uint8_t* reserved_data = (uint8_t*) malloc(header.biSizeImage);

    zeroficate_data(reserved_data, header.biSizeImage);

    image_load(img, (*img).width, (*img).height, padding, reserved_data);

    fseek(out, 54, 0);

    if (fwrite(reserved_data, 1, header.biSizeImage, out) != header.biSizeImage) {
        free(reserved_data);
        return WRITE_ERROR;
    }
    free(reserved_data);
    return WRITE_OK;
}

