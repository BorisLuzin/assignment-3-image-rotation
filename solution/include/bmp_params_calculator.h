#ifndef IMAGE_TRANSFORMER_BMP_PARAMS_CALCULATOR_H
#define IMAGE_TRANSFORMER_BMP_PARAMS_CALCULATOR_H
#include <stdint.h>
#include <stdio.h>
#define SIZE_OF_RGB_PIXEL 3
#define COUNT_OF_BIT_IN_BATCH 4

static uint32_t calculate_p_size(uint32_t pixel_param) {
    return pixel_param * SIZE_OF_RGB_PIXEL;
}

uint32_t calculate_bytes_padding(uint32_t width) {
    uint32_t w_size = calculate_p_size(width);

    if (w_size % COUNT_OF_BIT_IN_BATCH == 0) {
        return 0;
    }
    else {
        uint32_t padding_shift = COUNT_OF_BIT_IN_BATCH - (w_size % COUNT_OF_BIT_IN_BATCH);
        return padding_shift % COUNT_OF_BIT_IN_BATCH;
    }
}

uint32_t calculate_image_size(uint32_t width, uint32_t height) {
    return (calculate_p_size(width) + calculate_bytes_padding(width)) * height;
}

uint32_t current_address_from_coordinates(uint32_t width, uint32_t padding, uint32_t row, uint32_t col) {
    uint32_t x = (calculate_p_size(width) + padding) * (row);
    uint32_t y = calculate_p_size(col);
    return (x + y);
}

#endif //IMAGE_TRANSFORMER_BMP_PARAMS_CALCULATOR_H
