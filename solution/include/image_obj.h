#ifndef IMAGE_TRANSFORMER_IMAGE_OBJ_H
#define IMAGE_TRANSFORMER_IMAGE_OBJ_H
#include <inttypes.h>

struct pixel {
    uint8_t blue;    // Компонента синего цвета
    uint8_t green;   // Компонента зеленого цвета
    uint8_t red;     // Компонента красного цвета
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};



#endif //IMAGE_TRANSFORMER_IMAGE_OBJ_H
