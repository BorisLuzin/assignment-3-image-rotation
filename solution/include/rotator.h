#ifndef IMAGE_TRANSFORMER_ROTATOR_H
#define IMAGE_TRANSFORMER_ROTATOR_H
#include <stdint.h>
#include <stdlib.h>
struct image rotator(struct image* source, int64_t radian);
#endif //IMAGE_TRANSFORMER_ROTATOR_H
