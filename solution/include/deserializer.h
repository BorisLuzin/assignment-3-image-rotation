#ifndef IMAGE_TRANSFORMER_BMP_TRANSFORM_H
#define IMAGE_TRANSFORMER_BMP_TRANSFORM_H
#include "../include/image_obj.h"
#include <stdio.h>
#include <stdlib.h>

void image_init(struct image* img, uint64_t width, uint64_t height, struct pixel* data);

enum read_status  {
    READ_OK = 0,
    READ_INVALID_RADIANS = 1,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
    /* коды других ошибок  */
};

enum read_status from_bmp(FILE* in, struct image* img);

/*  serializer   */
enum write_status  {
    WRITE_OK = 0,
    WRITE_ERROR = 1,
    /* коды других ошибок  */
};


enum write_status to_bmp(FILE* out, struct image* img);
#endif //IMAGE_TRANSFORMER_BMP_TRANSFORM_H
